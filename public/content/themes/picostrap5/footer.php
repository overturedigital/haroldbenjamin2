</main>
	<?php if (function_exists("lc_custom_footer")) lc_custom_footer(); else {
		?>
		<?php if (is_active_sidebar( 'footerfull' )): ?>
		<div class="wrapper bg-light mt-5 py-5" id="wrapper-footer-widgets">
			
			<div class="container mb-5">
				
				<div class="row">
					<?php dynamic_sidebar( 'footerfull' ); ?>
				</div>

			</div>
		</div>
		<?php endif ?>
		
		
		<div class="wrapper py-3" id="wrapper-footer-colophon">
			<div class="container-fluid">
		
				<div class="row">
		
					<div class="col text-center">
		
						<footer class="site-footer" id="colophon">
		
							<div class="site-info">
		
								<?php picostrap_site_info(); ?>
		
							</div><!-- .site-info -->
		
						</footer><!-- #colophon -->
		
					</div><!--col end -->
		
				</div><!-- row end -->
		
			</div><!-- container end -->
		
		</div><!-- wrapper end -->
		
	<?php 
	} //END ELSE CASE ?>
	<script>
            var map, map2;

            function initMap() {
                var styles = [{
                        "featureType": "administrative",
                        "elementType": "all",
                        "stylers": [{
                            "saturation": "-100"
                        }]
                    },
                    {
                        "featureType": "administrative.province",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                                "saturation": -100
                            },
                            {
                                "lightness": 65
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [{
                                "saturation": -100
                            },
                            {
                                "lightness": "50"
                            },
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [{
                            "saturation": "-100"
                        }]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "all",
                        "stylers": [{
                            "lightness": "30"
                        }]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "all",
                        "stylers": [{
                            "lightness": "40"
                        }]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [{
                                "saturation": -100
                            },
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{
                                "hue": "#ffff00"
                            },
                            {
                                "lightness": -25
                            },
                            {
                                "saturation": -97
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels",
                        "stylers": [{
                                "lightness": -25
                            },
                            {
                                "saturation": -100
                            }
                        ]
                    }
                ];

                map = new google.maps.Map(document.getElementById("googleMap1"), {
                    center: {
                        lat: 51.5802688,
                        lng: -0.3374085
                    },
                    zoom: 17,
                    styles: styles,
                    scrollwheel: false,
                    mapTypeControl: false,
                    streetViewControl: false,
                });

                map2 = new google.maps.Map(document.getElementById("googleMap2"), {
                    center: {
                        lat: 51.5802721,
                        lng: -0.3395972
                    },
                    zoom: 17,
                    styles: styles,
                    scrollwheel: false,
                    mapTypeControl: false,
                    streetViewControl: false,
                });
                



                //for animation marker drop
                // animation: google.maps.Animation.DROP;

                var markerlatlng = new google.maps.LatLng(51.5802688, -0.3374085);

                marker = new google.maps.Marker({
                    position: markerlatlng,
                    map: map,
                    icon: {
                        url: "<?php echo get_template_directory_uri(); ?>/assets/img/mapmarker.png",
                        scaledSize: new google.maps.Size(35, 51),
                        anchor: new google.maps.Point(18, 51),
                    },
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                });

                marker.setMap(map);                

                var markerlatlng2 = new google.maps.LatLng(51.5802721, -0.3395972);

                marker2 = new google.maps.Marker({
                    position: markerlatlng2,
                    map: map2,
                    icon: {
                        url: "<?php echo get_template_directory_uri(); ?>/assets/img/mapmarker.png",
                        scaledSize: new google.maps.Size(35, 51),
                        anchor: new google.maps.Point(18, 51),
                    },
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                });

                marker2.setMap(map2);                
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-ugiAsJZDhkfps0LhTkxmhoeDT2ata7Q&callback=initMap" async defer></script>

	<?php wp_footer(); ?>

	</body>
</html>