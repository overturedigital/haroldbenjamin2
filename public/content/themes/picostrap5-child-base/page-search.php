<?php
/*
Template name: Page - Search
*/
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$q = $_GET['q'];
$link = '';
$flag = 0;
$blog_count = 0;
$blog_contents = '';
$post_count = 0;
$post_contents = '';
$people_count = 0;
$people_contents = '';
?>

<?php
//search on blogs
$args = array(
    'post_type' => 'blog',
    'posts_per_page' => -1,
);
$wp_query = new WP_Query( $args );

if ( $wp_query->have_posts() ) {
    ob_start();
    echo '<div class="container blog_container show">';
    echo '<div class="col-lg-10 offset-lg-2">';
    while ( $wp_query->have_posts() ) {
        $wp_query->the_post(); 
        $content = apply_filters( 'the_content', get_the_content() );
        if (stripos($content, $q) > 0) {
            $blog_count ++;
            $flag = 1;
            $link = get_permalink();
            $replace_txt = '<span class="highlight" style="background-color: #009fd6;">' . $q . '</span>';
            $content = str_ireplace($q, $replace_txt, $content);
            $txt_array = explode("<h3>",$content);
            $txt_array = explode("<p>",$txt_array[0]);
            $content = '';
            foreach($txt_array as $para){
                if(strpos($para, $q) > 0){
                    $para = str_replace('</p>', '', $para);
                    $para = str_replace('<p>', '', $para);
                    $temp = '<p>' . $para . '</p>';
                    $content .= $temp;
                }
            }           
            ?>
            <div class="mb-4">
                <div class="col-12 col-md-12 col-lg-12 offset-lg-0">
                    <div class="row">
                        <a class="title_link" href="<?php echo $link;?>" style="text-decoration: none;">
                            <h1 class="search-result-title" style="margin-top: 60px;"><?php the_title(); ?></h1>
                        </a>                    
                    </div>
                </div>
            </div>        
            <div class="main-content">
                <div class="col-12 col-md-12 col-lg-12 offset-lg-0">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php echo $content; ?>
                        </div>
                    </div>
                    <div class="row btn" style="margin-left: 0.5rem;">
                        <a class="more_link" href="<?php echo $link;?>">Read More...</a>
                    </div>
                </div>
            </div>
        <?php        
        }
    }   
    echo '</div></div>';
    $blog_contents = ob_get_contents();
    ob_end_clean();
}

//search on news
$args = array(
    'post_type' => 'post',
    'posts_per_page' => -1,
);
$post_query = new WP_Query( $args );
if ( $post_query->have_posts() ) {
    ob_start();
    echo '<div class="container post_container show">';
    echo '<div class="col-lg-10 offset-lg-2">';
    while ( $post_query->have_posts() ) {
        $post_query->the_post(); 
        $content = apply_filters( 'the_content', get_the_content() );
        if (stripos($content, $q) > 0) {
            $post_count ++;
            $flag = 1;
            $link = get_permalink();
            $replace_txt = '<span class="highlight" style="background-color: #009fd6;">' . $q . '</span>';
            $content = str_replace($q, $replace_txt, $content);
            $txt_array = explode("<h3>",$content);
            $txt_array = explode("<p>",$txt_array[0]);
            $content = '';
            foreach($txt_array as $para){
                if(stripos($para, $q) > 0){
                    $para = str_replace('</p>', '', $para);
                    $para = str_replace('<p>', '', $para);
                    $temp = '<p>' . $para . '</p>';
                    $content .= $temp;
                }
            }
            ?>
            <div class="mb-4">
                <div class="col-12 col-md-12 col-lg-12 offset-lg-0">
                    <div class="row">
                        <a class="title_link" href="<?php echo $link;?>" style="text-decoration: none;"><h1 class="search-result-title" style="margin-top: 60px;"><?php the_title(); ?></h1></a>
                    </div>
                </div>
            </div>        
            <div class="main-content">
                <div class="col-12 col-md-12 col-lg-12 offset-lg-0">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php echo $content; ?>
                        </div>
                    </div>
                    <div class="row btn" style="margin-left: 0.5rem;">
                        <a class="more_link" href="<?php echo $link;?>">Read More...</a>
                    </div>
                </div>
            </div>
        <?php
        }
    }   
    echo '</div></div>';
    $post_contents = ob_get_contents();
    ob_end_clean();
}    

//search on people
$args = array(
    'post_type' => 'people',
    'posts_per_page' => -1,
);
$people_query = new WP_Query( $args );

$content = '';
if ( $people_query->have_posts() ) {
    ob_start();
    echo '<div class="container people_container show">';
    echo '<div class="col-lg-10 offset-lg-2">';
    while ( $people_query->have_posts() ) {
        $people_query->the_post(); 
        $content = get_field('introduction') . get_field('maintext') . get_field('education');
        if (stripos($content, $q) > 0) {
            $people_count ++;
            $flag = 1;
            $link = get_permalink();
            $replace_txt = '<span class="highlight" style="background-color: #009fd6;">' . $q . '</span>';
            $content = str_replace($q, $replace_txt, $content);
            $txt_array = explode("<h3>",$content);
            $txt_array = explode("<p>",$txt_array[0]);
            $content = '';
            foreach($txt_array as $para){
                if(stripos($para, $q) > 0){
                    $para = str_replace('</p>', '', $para);
                    $para = str_replace('<p>', '', $para);
                    $temp = '<p>' . $para . '</p>';
                    $content .= $temp;
                }
            }
            ?>
            <div class="mb-4">
                <div class="col-12 col-md-12 col-lg-12 offset-lg-0">
                    <div class="row">
                        <a class="title_link" href="<?php echo $link;?>" style="text-decoration: none;"><h1 class="search-result-title" style="margin-top: 60px;"><?php the_title(); ?></h1></a>
                    </div>
                </div>
            </div>        
            <div class="main-content">
                <div class="col-12 col-md-12 col-lg-12 offset-lg-0">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php echo $content; ?>
                        </div>
                    </div>
                    <div class="row btn" style="margin-left: 0.5rem;">
                        <a class="more_link" href="<?php echo $link;?>">Read More...</a>
                    </div>
                </div>
            </div>
        <?php
        }
    }   
    echo '</div></div>';
    $people_contents = ob_get_contents();
    ob_end_clean();
}
?>
<div class="container">
    <div class="col-lg-10 offset-lg-2 pt-6">
        <div class="row flex main_search_w">
            <div class="search-container" id="main_search-container">
                <form method="GET" action="/search/" id="search-form" name="search-form" class="search-form">
                    <div class="search-bar" style="">
                        <input placeholder="Search..." type="text" class="search-input" id="search-input" value="<?php echo $q;?>" name="q" size="10" style="border: 1px solid #000000; background-color: #FFFFFF; height:38px; width: 70%;">
                        <input type="hidden" name="m" value="any">
                        <button class="search_btn btn" style="border:0 !important;" id="search-submit" name="search-submit" type="submit" class="search-submit">
                            <i style="font-size:18px" class="fa">&#xf002;</i>
                        </button>
                    </div>
                </form>
            </div>
            <?php if($flag == 1){
                echo '<div class="tab">
                        <button class="tablinks tab_all active btn" data-tab="all">All(' . ($blog_count + $post_count + $people_count) . ')</button>';    
                        if($post_count > 0)    
                            echo '<button class="tablinks tab_post btn" data-tab="post_container">News(' . $post_count . ')</button>';
                        if($blog_count > 0)
                            echo '<button class="tablinks tab_blog btn" data-tab="blog_container">Blogs(' . $blog_count . ')</button>';
                        if($people_count > 0)
                            echo '<button class="tablinks tab_people btn" data-tab="people_container">People(' . $people_count . ')</button>';
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>
<?php
if($flag == 1){
    echo '<div class="container">
            <div class="col-lg-10 offset-lg-2 pt-6">
                <div class="row">
                    <p class="search-stats" id="search-stats">Showing <strong><em>' . ($blog_count + $post_count + $people_count) . '</em></strong> results for your search phrase, organised below. Use the buttons to filter by content type.</p>
                </div>
            </div>
        </div>';
}
else{
    echo '<div class="container">
            <div class="col-lg-10 offset-lg-2 pt-6">
                <div class="row">
                    <p class="search-stats none-found" id="search-stats">Sorry, no results were found for the search term(s). Suggestions:</p>
                    <ul class="search-stats">
                        <li> Make sure all words are spelled correctly</li>
                        <li> Try different keywords</li>
                        <li> Try more general keywords</li>
                    </ul>
                    
        </div></div></div>';
}

if($post_count > 0){
    echo $post_contents;
}
if($blog_count > 0){
    echo $blog_contents;
}
if($people_count > 0){
    echo $people_contents;
}

get_footer();
