<?php
get_header();
global $post;
$post_id = $post->ID;
$image = get_field('field_61239b50daccc', $post_id);
$bg_color = get_field('field_61239b50daddd', $post_id);
$bg_img = get_the_post_thumbnail_url($post_id, 'full');
$txt_color_class = '';    
if(strpos($bg_img, "lightblue-bg.png") || strpos($bg_img, "2021/11/image.png")){
  $txt_color_class = "color_bk";  
}
$term_list = wp_get_post_terms( $post_id, 'category', array( 'fields' => 'names' ) );
?>
<!-- portrait image (NOT THUMBNAIL/featured) to be background image -->
<section id="start" class="section-sticky section-sticky fadeinQuick" style="background-color:<?php echo $bg_color; ?>" ><img src="<?php echo $image['url']; ?>">
	<div class="hero-sticky-top"  style="background-color:<?php echo $bg_color; ?>">
		<div class="container">
			<div class="col-lg-10 offset-lg-2">
				<div class="row">
					<div class="<?php echo $txt_color_class;?> col-12 col-md-12 col-lg-10 offset-lg-0 pt-6" editable="rich">
                  	<h2><?php the_title(); ?></h2>
                    <p> <?php echo get_the_date('d M Y'); ?><br>
                    <a href="<?php echo (get_field('post_person'))->post_title ? get_permalink((get_field('post_person'))->ID) : ''; ?>"><?php echo get_the_author_meta('display_name', $post->post_author); ?></a></p>
                    <?php
                    if($term_list){
                      	echo '<p>';
                      	$count = 0;
                      	foreach($term_list as $term){
							$category_id = get_cat_ID($term);
                      		$category_link = get_category_link( $category_id );
                          	//replace category page link with the static page link
                          	$category_link = str_replace('https://haroldbenjamin2.ovstaging.com/category', '', $category_link);	                              
                          	if($count == 0)
                            	echo '<a href="' . $category_link . '">' . $term . '</a>';
                          	else
                              	echo ', <a href="' . $category_link . '">' . $term . '</a>';
                          	$count++;
                      	}
                      	echo '</p>';                     
                    }
                    ?>
					<p class="lead">   <?php the_content(); ?></p>		
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--      BOTTOM part :       -->
<?php
if (get_field('introduction') || get_field('education') || get_field('right_column_content')){
?>
    <section>
        <div class="container" style="position:relative;">
    		<div class="col-lg-10 offset-lg-2">
                <?php 
                if(get_field('introduction')){
                ?>
                    <div class="row">
        				<div class="col-12 col-md-5 col-lg-12 text-dark">
                            <p> <?php echo get_field('introduction'); ?></p>
                        </div>
                    </div>
                <?php
                }
                if(get_field('education') || get_field('right_column_content')){
                ?>
                    <div class="row">
        				<div class="col-12 col-md-5 col-lg-5 text-dark">
                            <p> <?php echo get_field('education'); ?></p>
        				</div>
        				<div class="col-12 col-md-5 col-lg-4 text-dark">
        					<p><?php echo get_field('right_column_content'); ?> </p>
        				</div>
        			</div>
			    <?php
			    }
			    ?>
    		</div>
    	</div>
    </section>
<?php
} 
?>
<section  style="padding:0 0 32px 0;background-color:<?php echo $bg_color;?>" class="<?php  echo $txt_color_class;?>">
  <div class="container" style="display: flex;">
	<div class="col-lg-6 col-md-6 offset-lg-2 ">
    	<?php echo get_previous_post_link( '%link', '<span>←</span>Prev' );?>	      
    </div>
    <div class="col-lg-6 col-md-6 offset-lg-2">
    	<?php echo get_next_post_link( '%link', 'Next<span>→</span>' );?>
    </div>
  </div>
</section> 
<!--
<section style="background-color: #444;">
	<h3>Related Post</h3>
    <div class="row">
      	<?php 
        $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => -1, 'post__not_in' => array($post->ID) ) );
    		if( $related ) {
          	foreach( $related as $post ) {
              setup_postdata($post); ?>
              <div class="col-sm-3">
                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                <?php the_excerpt(); ?>
              </div>
              <?php 
        	}    
        }
    		wp_reset_postdata(); 
        ?>
    </div>
</section>
        -->
<?php
get_footer();

