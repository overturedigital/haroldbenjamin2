// to enable the enqueue of this optional JS file, 
// you'll have to uncomment a row in the functions.php file
// just read the comments in there mate



//add here your own js code. Vanilla JS welcome.
jQuery(document).ready(function(){
    console.log("Custom js loaded");
    jQuery(".staff-search-button").click(function(e){
        jQuery(".search_result").html('');
        let key_origin = jQuery("#ss").val();
        let key = key_origin.toLowerCase();
        let department_origin = jQuery("#department-search option:selected").text();
        let department = department_origin.toLowerCase();
        let count = 0;
        let items = jQuery("section:not('#start, #search_section')");
        items.each(function(){
            let section = false;
            let self = jQuery(this);
            let sec_items = self.find(".people-listitem");
            sec_items.each(function(){
                let item = jQuery(this).find(".title");
                let depart_w = jQuery(this).find(".depart");
                let name = item.find("a").text().toLowerCase();
                let depart = depart_w.text().toLowerCase();
                let match_name = false;
                let match_depart = false;
                //check search box
                if(name.indexOf(key) == -1){
                    if(depart_w.length > 0){
                        if(depart.indexOf(key) >= 0){
                            match_name = true;
                        }
                    }
                }
                else{
                    match_name = true;
                }
                //check department
                if(department == "any department"){
                    match_depart = true;
                }
                else if(depart_w.length > 0){
                    if(depart.indexOf(department) >= 0 ){
                        match_depart = true;
                    }
                }
                if(match_name == true && match_depart == true){
                    section = true;
                    jQuery(this).css("display", "block");
                    count++;
                } 
                else{
                    jQuery(this).css("display", "none");
                }
            });
            if(section == false){
                self.css("display", "none");
            }
            else{
                self.css("display", "block");
            }
            
        })
        if(count == 0){
            let text = '';
            if(key == ''){
                text = "Sorry, we were unable to find any results for " + "<strong><em>" + department_origin + "</em></strong>";
            }
            else{
                text = "Sorry, we were unable to find any results for " + "<strong><em>" + key_origin + ", " + department_origin + "</em></strong>";    
            }
            jQuery(".search_result").html(text);
        }
        else{
            let text = '';
            if(key == ''){
                text = "<strong>" + count + "</strong>" + " results found for " + "<strong><em>" + department_origin + "</em></strong>";
            }
            else{
                text = "<strong>" + count + "</strong>" + " results found for " + "<strong><em>" + key_origin + ", " + department_origin + "</em></strong>";    
            }
            jQuery(".search_result").html(text);
        }
    });

    jQuery('#menu-primary .menu-item.menu-item-has-children').mouseover(function(){        
        let child = jQuery(this).find('.dropdown-menu:not(.submenu)');
        child.addClass('show');
    });
    jQuery('#menu-primary .menu-item.menu-item-has-children').mouseout(function(){        
        let child = jQuery(this).find('.dropdown-menu:not(.submenu)');
        child.removeClass('show');
    });
    
    jQuery('#menu-primary .menu-item.menu-item-has-children .menu-item.menu-item-has-children').mouseover(function(){        
        let child = jQuery(this).find('.dropdown-menu.submenu');
        child.addClass('show');
    });
    jQuery('#menu-primary .menu-item.menu-item-has-children .menu-item.menu-item-has-children').mouseout(function(){        
        let child = jQuery(this).find('.dropdown-menu.submenu');
        child.removeClass('show');
    });

    let hero_top = 0;
    let admin_bar = 0;
    if(jQuery(".push-header-up .container").length ){
        hero_top = jQuery(".push-header-up .container").offset().top;
    }
    if(jQuery("#wpadminbar").length ){
        admin_bar = jQuery("#wpadminbar").height();
    }
    let device_w = jQuery( window ).width();
    jQuery(document).scroll(function(){
        let live_position = jQuery(window).scrollTop();
        let dist = hero_top - live_position;   
        if(jQuery("#wpadminbar").length ){
            admin_bar = jQuery("#wpadminbar").height();
        }
        if( device_w > 767 && hero_top > 0 ){
            if(dist < 136){
                jQuery("#lc-header").css("top", dist-136);
                jQuery("main#theme-main").css( "padding-top", dist-admin_bar);    
            }
            else{
                jQuery("#lc-header").css("top", admin_bar);
                jQuery("main#theme-main").css( "padding-top", 136-admin_bar);
            }
        }
    });

    //Search result
    jQuery('.tab_all').on('click', function(){
        jQuery(this).addClass('active');
        jQuery('.post_container').addClass('show');    
        jQuery('.blog_container').addClass('show');
        jQuery('.people_container').addClass('show');
        jQuery('.tab_blog').removeClass('active');
        jQuery('.tab_post').removeClass('active');
        jQuery('.tab_people').removeClass('active');
    });
    jQuery('.tab_post').on('click', function(){
        jQuery(this).addClass('active');
        jQuery('.post_container').addClass('show');    
        jQuery('.blog_container').removeClass('show');
        jQuery('.people_container').removeClass('show');
        jQuery('.tab_all').removeClass('active');
        jQuery('.tab_blog').removeClass('active');
        jQuery('.tab_people').removeClass('active');
    });
    jQuery('.tab_blog').on('click', function(){
        jQuery(this).addClass('active');
        jQuery('.blog_container').addClass('show');    
        jQuery('.post_container').removeClass('show');
        jQuery('.people_container').removeClass('show');
        jQuery('.tab_all').removeClass('active');
        jQuery('.tab_post').removeClass('active');
        jQuery('.tab_people').removeClass('active');
    });
    jQuery('.tab_people').on('click', function(){
        jQuery(this).addClass('active');
        jQuery('.people_container').addClass('show');    
        jQuery('.post_container').removeClass('show');
        jQuery('.blog_container').removeClass('show');
        jQuery('.tab_all').removeClass('active');
        jQuery('.tab_post').removeClass('active');
        jQuery('.tab_blog').removeClass('active');
    });
    
});

