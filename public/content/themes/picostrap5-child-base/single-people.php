<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();




if ( have_posts() ) : 
    while ( have_posts() ) : the_post();
    
    ?>
    
    <div class="team-member-profile mb-4" style="background-image: url('<?php echo get_field('profile_image')['url']; ?>')">
        <div class="team-member-profile-section-one">
            <h2 class="team-member-list">Our team</h2>
            <h1 class="team-member-title"><?php the_title(); ?></h1>
            <h3 class="team-member-role"><?php echo get_field('role'); ?></h3>
        </div>

        <div class="team-member-profile-section-two">
            <h6 class="team-member-title"><?php the_title(); ?></h6>
            <h6 class="team-member-role"><?php echo get_field('role'); ?></h6>
            <?php
                if($telephone = get_field('telephone')){
                    ?>
                        <p>T: <?php echo $telephone; ?></p>
                    <?php 
                }
                if($mobile = get_field('mobile')){
                    ?>
                        <p>M: <?php echo $mobile; ?></p>
                    <?php 
                }
                if($email = get_field('email')){
                    ?>
                        <p>E: <?php echo $email; ?></p>
                    <?php 
                }
            ?>
        </div>
    </div>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12">
                <?php the_content(); ?>
            </div>
        </div>
    </div>

<?php
    endwhile;
 else :
     _e( 'Sorry, no posts matched your criteria.', 'picostrap' );
 endif;
 ?>


 

<?php get_footer();
