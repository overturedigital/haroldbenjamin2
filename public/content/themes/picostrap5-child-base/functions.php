<?php

include_once 'inc/scss-compiler.php';
include_once 'inc/config-walker.php';

// SET A SPECIFIC DESTINATION FOLDER FOT THE COMPILED CSS BUNDLES
function picostrap_get_css_optional_subfolder_name() { return "css-output/"; }

// SET A CUSTOM NAME FOR THE CSS BUNDLE FILE
function picostrap_get_base_css_filename() { return "bundle.css"; }

// DISABLE APPLICATION PASSWORDS for security
add_filter( 'wp_is_application_passwords_available', '__return_false' );

// LOAD CHILD THEME TEXTDOMAIN
//add_action( 'after_setup_theme', function() { load_child_theme_textdomain( 'picostrap-child', get_stylesheet_directory() . '/languages' ); } );

// OPTIONAL ADDITIONAL CSS FILE - [NOT RECOMMENDED]: USE the /sass folder, add your css code to /sass/_custom.sass
add_action( 'wp_enqueue_scripts',  function  () {	
	// wp_enqueue_style( 'custom', get_stylesheet_directory_uri().'/custom.css' ); 
	wp_enqueue_style( 'menukit', get_stylesheet_directory_uri().'/css/menukit.css' ); 
});

// OPTIONAL ADDITIONAL JS FILE - just uncomment the row below
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_script('custom', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), null, true); 
	wp_enqueue_script('menukit', get_stylesheet_directory_uri() . '/js/menukit.js', array(/* 'jquery' */), null, true); 
});
 
// OPTIONAL: ADD FONTAWESOME FROM CDN IN FOOTER 
/* 
add_action("wp_footer",function(){ ?> <link rel='stylesheet' id='fontawesome-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' media='all' /> <?php }); 
*/

//OPTIONAL: ADD ANOTHER CUSTOM GOOGLE FONT, EXAMPLE: Hanalei Fill
// After uncommenting the following code, you will also need to set the font in the BS variable. Here's how:
// Open the WordPress Customizer 
// In the field/s: "Font Family Base" or "Headings Font Family" enter the font name, in this case "Hanalei Fill"

/*
add_action("wp_head",function(){ ?> 
 <link rel="dns-prefetch" href="//fonts.googleapis.com">
 <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
 <link href="https://fonts.googleapis.com/css?family=Hanalei+Fill" rel="stylesheet">
<?php }); 
*/

// OPTIONAL: ADD MORE NAV MENUS
//register_nav_menus( array( 'third' => __( 'Third Menu', 'picostrap' ), 'fourth' => __( 'Fourth Menu', 'picostrap' ), 'fifth' => __( 'Fifth Menu', 'picostrap' ), ) );
// THEN USE SHORTCODE:  [lc_nav_menu theme_location="third" container_class="" container_id="" menu_class="navbar-nav"]

function picostrap_alter_scss($the_scss_code){

	global $wp_filesystem;
	if (empty($wp_filesystem)) {
		require_once (ABSPATH . '/wp-admin/includes/file.php');
		WP_Filesystem();
	}

	$the_scss_code .= ' ';
	$the_scss_code .= $wp_filesystem->get_contents(WP_CONTENT_DIR . '/themes/'.get_stylesheet().'/css/custom.css');  
	$the_scss_code .= ' ';
	$the_scss_code .= $wp_filesystem->get_contents(WP_CONTENT_DIR . '/themes/'.get_stylesheet().'/css/menu.css');  
	$the_scss_code .= ' ';
	$the_scss_code .= $wp_filesystem->get_contents(WP_CONTENT_DIR . '/themes/'.get_stylesheet().'/css/form.css');  
	$the_scss_code .= ' ';
	$the_scss_code .= $wp_filesystem->get_contents(WP_CONTENT_DIR . '/themes/'.get_stylesheet().'/style.css');  
	$the_scss_code .= ' ';
	return $the_scss_code;
}

add_filter( 'wp_nav_menu_args', 'config_wp_nav_menu_args' );
function config_wp_nav_menu_args($args){
	$args['depth'] = 5;
	$args['walker'] = new config_walker();
	// unset($args['walker']);

	return $args;
}

add_action( 'init', 'cptui_register_my_cpts_team_members' );
function cptui_register_my_cpts_team_members() {

	/**
	 * Post Type: People.
	 */

	$labels = [
		"name" => __( "People", "picostrap-child-base" ),
		"singular_name" => __( "Person", "picostrap-child-base" ),
	];

	$args = [
		"label" => __( "People", "picostrap-child-base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "people", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "page-attributes" ],
		"show_in_graphql" => false,
	];

	register_post_type( "people", $args );

	/**
	 * Post Type: Project.
	 */

	$labels = [
		"name" => __( "Projects", "picostrap-child-base" ),
		"singular_name" => __( "Project", "picostrap-child-base" ),
	];

	$args = [
		"label" => __( "Project", "picostrap-child-base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "project", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "excerpt" ],
		"show_in_graphql" => false,
	];

	register_post_type( "project", $args );

	register_post_type( "project", $args );
	
	/**
	 * Post Type: Blog.
	 */

	$labels = [
		"name" => __( "Blogs", "picostrap-child-base" ),
		"singular_name" => __( "Blog", "picostrap-child-base" ),
	];

	$args = [
		"label" => __( "Blog", "picostrap-child-base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "blog", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "author", "thumbnail", "excerpt", "page-attributes" ],
		"show_in_graphql" => false,
	];

	register_post_type( "blog", $args );
	
	/**
	 * Post Type: Service.
	 */

	$labels = [
		"name" => __( "Services", "picostrap-child-base" ),
		"singular_name" => __( "Service", "picostrap-child-base" ),
	];

	$args = [
		"label" => __( "Service", "picostrap-child-base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "service", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "author", "thumbnail", "excerpt", "page-attributes" ],
		"show_in_graphql" => false,
	];

	register_post_type( "service", $args );
	
	/**
	 * Post Type: Sector.
	 */

	$labels = [
		"name" => __( "Sectors", "picostrap-child-base" ),
		"singular_name" => __( "Sector", "picostrap-child-base" ),
	];

	$args = [
		"label" => __( "Sector", "picostrap-child-base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "sector", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "author", "thumbnail", "excerpt", "page-attributes" ],
		"show_in_graphql" => false,
	];

	register_post_type( "sector", $args );
	
	/**
	 * Post Type: Testimonial.
	 */

	$labels = [
		"name" => __( "Testimonials", "picostrap-child-base" ),
		"singular_name" => __( "Testimonial", "picostrap-child-base" ),
	];

	$args = [
		"label" => __( "Testimonial", "picostrap-child-base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "testimonial", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "author", "thumbnail", "excerpt", "page-attributes" ],
		"show_in_graphql" => false,
	];

	register_post_type( "testimonial", $args );
}

add_action( 'init', 'cptui_register_my_taxes_group' );
function cptui_register_my_taxes_group() {

	/**
	 * Taxonomy: Teams.
	 */

	$labels = [
		"name" => __( "Teams", "picostrap-child-base" ),
		"singular_name" => __( "Team", "picostrap-child-base" ),
	];

	
	$args = [
		"label" => __( "Teams", "picostrap-child-base" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'team', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "team",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		"show_in_graphql" => false,
	];
	register_taxonomy( "team", [ "people" ], $args );

	/**
	 * Taxonomy: Departments.
	 */

	$labels = [
		"name" => __( "Departments", "picostrap-child-base" ),
		"singular_name" => __( "Department", "picostrap-child-base" ),
	];

	
	$args = [
		"label" => __( "Departments", "picostrap-child-base" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'department', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "department",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		"show_in_graphql" => false,
	];
	register_taxonomy( "department", [ "people" ], $args );

	/**
	 * Taxonomy: Types.
	 */

	$labels = [
		"name" => __( "Types", "picostrap-child-base" ),
		"singular_name" => __( "Type", "picostrap-child-base" ),
	];

	
	$args = [
		"label" => __( "Types", "picostrap-child-base" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'project-type', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "project-type",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		"show_in_graphql" => false,
	];
	register_taxonomy( "project_type", [ "project" ], $args );	

}

add_filter( 'manage_edit-team_columns', 'config_add_group_columns' );
function config_add_group_columns( $columns ) {
    $columns['shortcode'] = 'Shortcode';
    return $columns;
}

add_filter( 'manage_edit-project_type_columns', 'config_add_project_type_columns' );
function config_add_project_type_columns( $columns ) {
    $columns['shortcode'] = 'Shortcode';
    return $columns;
}

add_filter('manage_team_custom_column', 'confgi_add_group_column_content',10,3);
function confgi_add_group_column_content($content,$column_name,$term_id){
    $term = get_term($term_id, 'team');
    switch ($column_name) {
        case 'shortcode':
            //do your stuff here with $term or $term_id
            $content = '[config-team team="'. $term->slug .'"]';
            break;
        default:
            break;
    }
    return $content;
}

add_filter('manage_project_type_custom_column', 'confgi_add_project_type_column_content',10,3);
function confgi_add_project_type_column_content($content,$column_name,$term_id){
    $term = get_term($term_id, 'project_type');
    switch ($column_name) {
        case 'shortcode':
            //do your stuff here with $term or $term_id
            $content = '[config-projects type="'. $term->slug .'"]';
            break;
        default:
            break;
    }
    return $content;
}

add_shortcode('config-team', 'config_team_shortcode');
function config_team_shortcode($atts){
	$default = array('team' => false);
	$atts = shortcode_atts($default, $atts, 'config-team');

	extract($atts);

	$r = '';
	if($team){
		$team_members = get_posts(
		    array(
		        'posts_per_page' => -1,
		        'post_type' => 'people',
		        'tax_query' => array(
		            array(
		                'taxonomy' => 'team',
		                'field' => 'slug',
		                'terms' => $team,
		            )
		        )
		    )
		);

		if($team_members){
			ob_start();
			?>
				<div class="config-team">
					<div class="team-member-main-hp row">
					<?php 
						foreach ($team_members as $team_member) {
							?>
								<div class="col-lg-6" data-aos="fade-right">
									<img src="<?php echo get_the_post_thumbnail_url($team_member->ID, 'full'); ?>" class="d-block mx-lg-auto img-fluid" alt="" loading="lazy">
									<div class="lc-block  text-left">
										<h4 class=""><?php echo $team_member->post_title; ?></h4>
										<h6><?php echo get_field('role', $team_member->ID); ?></h6>
										<p class="small"><?php echo get_the_excerpt($team_member->post_content); ?></p>
									</div>
									<div class="lc-block  text-left">
										<a class="btn btn-outline-secondary" href="<?php echo get_permalink($team_member->ID); ?>" role="button">Read More</a>
									</div>
								</div>
								<!-- <div class="col-lg-3 col-md-3 col-sm-12 col-12 team-member-hp">
									<a href="javascript:;">
										<img src="<?php // echo get_the_post_thumbnail_url($team_member->ID, 'full'); ?>" alt="">
										<div class="team-member-hp-details">
											<h4><?php // echo $team_member->post_title; ?></h4>
											<h6><?php // echo get_field('role', $team_member->ID); ?></h6>
											<div class="details">
												<?php // echo do_shortcode($team_member->post_content); ?>
											</div>
										</div>
									</a>
								</div> -->
								<!-- <div class="instagram-block-main-hp">
									<div class="instagram-info-main-hp">
										<div class="instagram-title-bottom-hp text-center">
											<a href="<?php // echo get_field('instagram_link', $team_member->ID); ?>"><?php // echo get_field('instagram_id', $team_member->ID); ?></a>
										</div>
									</div>
								</div> -->
							<?php 
						}
					?>
					</div>
				</div>
			<?php 
			$r = ob_get_clean();
		}
	}

	return $r;
}

add_shortcode('config-projects', 'config_projects_shortcode');
function config_projects_shortcode($atts){
	$default = array('type' => false);
	$atts = shortcode_atts($default, $atts, 'config-projects');

	extract($atts);

	$r = '';
	if($type){
		$projects = get_posts(
		    array(
		        'posts_per_page' => -1,
		        'post_type' => 'project',
		        'tax_query' => array(
		            array(
		                'taxonomy' => 'project_type',
		                'field' => 'slug',
		                'terms' => $type,
		            )
		        )
		    )
		);

		if($projects){
			ob_start();
			?>
				<div class="config-projects">
					<div class="team-member-main-hp row">
					<?php 
						foreach ($projects as $project) {
							?>
								<div class="col-lg-3 col-md-3 col-sm-12 col-12 team-member-hp">
									<a href="javascript:;">
										<img src="<?php echo get_the_post_thumbnail_url($project->ID, 'full'); ?>" alt="">
										<div class="team-member-hp-details">
											<h4><?php echo $project->post_title; ?></h4>
											<h6><?php echo get_field('role', $project->ID); ?></h6>
											<div class="details">
												<?php echo do_shortcode($project->post_content); ?>
											</div>
										</div>
									</a>		
								</div>
							<?php 
						}
					?>
					</div>
				</div>
			<?php 
			$r = ob_get_clean();
		}
	}

	return $r;
}

add_shortcode('config-latest-posts', 'config_latest_posts_shortcode');
function config_latest_posts_shortcode($atts){
	$default = array('number' => 10);
	$atts = shortcode_atts($default, $atts, 'config-latest-posts');

	extract($atts);

	$r = '';
	if($number){
		$posts = get_posts(
		    array(
		        'posts_per_page' => $number,
		        'post_type' => 'post',
		        'orderby' => 'date',
		        'order' => 'DESC',
		    )
		);

		if($posts){
			ob_start();
			?>
				<div class="config-latest-posts">
					<div class="team-member-main-hp row">
					<?php 
						global $post;
						foreach ($posts as $post) {
							setup_postdata( $post );
							?>
								<div class="col-lg-6 col-md-6 col-sm-12 col-12 team-member-hp">
									<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="">
									<div class="team-member-hp-details">
										<h6 class="date">DATE // <?php echo get_the_date( get_option('date_format'), $post->ID ); ?></h6>
										<h6 class="author">AUTHOR // <?php echo get_the_author_meta( 'nicename', $post->post_author );; ?></h6>
										<h6 class="terms">
											<?php 
												if($terms = wp_get_post_terms($post->ID, ['post_tag'], ['fields' => 'names'])){
													echo '// ';
													echo implode(', ', $terms);
													echo ' //';
												}
											?>
										</h6>
										<h4 class="post-title"><?php echo $post->post_title; ?></h4>
										<!-- <div class="details">
											<?php // echo get_the_excerpt($post->ID); // do_shortcode($post->post_content); ?>
										</div> -->
										<div class="content">
											<a class="read-more-btn" href="<?php echo get_permalink($post->ID); ?>">[Read More]</a>
										</div>
									</div>		
								</div>
							<?php 
						}
						wp_reset_postdata();
					?>
					</div>
				</div>
			<?php 
			$r = ob_get_clean();
		}
	}

	return $r;
}

add_shortcode('config-show-more-less', 'config_show_more_less', 10, 2);
function config_show_more_less($atts, $content){
	$default = array('show_more_text' => 'Show More', 'show_less_text' => 'Show Less');
	$atts = shortcode_atts($default, $atts, 'config-show-more-less');
	extract($atts);

	$r = '';

	ob_start();
	?>
		<div class="config-show-more-less">
			<div class="show-more-text-box d-none">
				<?php echo do_shortcode($content); ?>
				<a class="show_less_text" href="javascript:;"><?php echo $show_less_text; ?></a>
			</div>
			<div class="show-less-text-box">
				<a class="show_more_text" href="javascript:;"><?php echo $show_more_text; ?></a>
			</div>
		</div>
	<?php 
	$r = ob_get_clean();

	return $r;
}

add_shortcode('testimonials', function(){
	//ob_start();
	
	$args = array(
    	'post_type'      => 'testimonial',
    	'posts_per_page' => -1,
    	'order'          => 'ASC',
    	'orderby'        => 'menu_order'
  	);
  	$tests = get_posts( $args );
  	$return = '<div id="carousel_testimonial" class="carousel slide" data-bs-ride="carousel"><div class="carousel-indicators">
						<button type="button" data-bs-target="#carousel_testimonial" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>';
  	if( $tests ){
  	$t=0;
  		foreach( $tests as  $test){
			if($t > 0){
				$return .= '<button type="button" data-bs-target="#carousel_testimonial" data-bs-slide-to="'.$t.'" aria-label="Slide '.$t.'"></button>';
			}
			$t++;
  		}
  	}

  	$return .= '</div>';

  	$return .= '<div class="carousel-inner">';
  	if( $tests ){
  		$t=1;
  		foreach( $tests as  $test){
			$post_id = $test->ID;
  		    $color = get_field('slider_color', $test->ID);  
			$bg_img = get_field('bg_image', $test->ID);
  			$active = '';
  			if($t == 1) $active = ' active';
  				$return .= '<div class="carousel-item'.$active.'">';
				$return .= '<div class="row carousel-caption-wrapper">';
				$return .= '<div class="align-self-center">';
				$return .= '<img class="d-block w-100 px-3 mb-3" src="' . $bg_img['url'] . '">';
				$return .= '</div>';
				$return .= '<div class="col-12 col-lg-6 left-side-wrap carousel-caption-inner-wrap" style="background-color:' . $color . '; height:400px;">';
				$return .= '<div style="position:relative;top:110px;left:22%;" class="carousel-caption-content">';
				$return .= '<p class="lead" style="width:56%">' . $test->post_content . '</p>';
				$return .= '</div>';
				$return .= '</div>';
				$return .= '<div class="col-6 col-lg-4 hidden_t"></div>';
				$return .= '<div class="col-6 col-lg-2 hidden_t carousel-caption-inner-wrap" style="background-color:' . $color . ';height:400px;"></div>';
				$return .= '</div>';
			$return .= '</div>';
            
            $t++;
  		}
  	}
	$return .= '</div></div>';
    //$return = ob_get_contents();
	//ob_clean();
    return $return;
});

add_action('wp_footer', 'config_wp_footer');
function config_wp_footer(){
	?>
		<script type="text/javascript">
			// to play videos in queue
			let video_tags = document.querySelectorAll('video.queue');
			if(video_tags){
				video_tags.forEach((video_tag, index, arr) => {

					video_tag.addEventListener('ended', function(e) {
					  	// get the active source and the next video source.
					  	// I set it so if there's no next, it loops to the first one

					  	let activesource = video_tag.querySelector('source.active');
					  	let nextsource = video_tag.querySelector('source.active + source') || (video_tag.src == '' ? video_tag.querySelector("source:nth-child(2)") : null)  || video_tag.querySelector("source:first-child");

					  	// deactivate current source, and activate next one
					  	if(activesource){
							activesource.className = "";
					  	}
						nextsource.className = "active";

						// update the video source and play
						video_tag.src = nextsource.src;
						video_tag.play();
					});
				});
			}

			// to add show/hide feature
			document.querySelectorAll('.config-show-more-less .show-less-text-box a.show_more_text').forEach(btn => {
		        btn.addEventListener('click', function(e){
		            e.preventDefault();

		            // var el = this.parentElement.nextElementSibling;
		            // el.classList.toggle('hide');
		            this.parentElement.classList.add('d-none');
		            this.parentElement.parentElement.querySelector('.show-more-text-box').classList.remove('d-none');
		        });
		    });

		    document.querySelectorAll('.config-show-more-less .show-more-text-box a.show_less_text').forEach(btn => {
		        btn.addEventListener('click', function(e){
		            e.preventDefault();

		            // var el = this.parentElement.nextElementSibling;
		            // el.classList.toggle('hide');
		            this.parentElement.classList.add('d-none');
		            this.parentElement.parentElement.querySelector('.show-less-text-box').classList.remove('d-none');
		        });
		    });
		</script>
	<?php 
}


// =================================================================
// for acf
// =================================================================

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_61239b459c775',
	'title' => 'People',
	'fields' => array(
		array(
			'key' => 'field_61239b50daeae',
			'label' => 'Role',
			'name' => 'role',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae1',
			'label' => 'Telephone',
			'name' => 'telephone',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae2',
			'label' => 'Mobile',
			'name' => 'mobile',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae3',
			'label' => 'Email',
			'name' => 'email',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b5adaeaf',
			'label' => 'Landing Image',
			'name' => 'landing_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_61239b78daeb0',
			'label' => 'Profile Image',
			'name' => 'profile_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_61239b50daeaez01',
			'label' => 'Maintext',
			'name' => 'maintext',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeaez04',
			'label' => 'Content',
			'name' => 'content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeaez02',
			'label' => 'Twitter',
			'name' => 'twitter',
			'type' => 'url',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeaez03',
			'label' => 'Linkedin',
			'name' => 'linkedin',
			'type' => 'url',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae33',
			'label' => 'Introduction',
			'name' => 'introduction',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae333',
			'label' => 'Experience',
			'name' => 'experience',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae123',
			'label' => 'CMS',
			'name' => 'cms',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae3333',
			'label' => 'Education',
			'name' => 'education',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'people',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_6124de0ba8df7',
	'title' => 'Projects',
	'fields' => array(
		array(
			'key' => 'field_6124de101dc7b',
			'label' => 'Landing Image',
			'name' => 'landing_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_6124de291dc7c',
			'label' => 'Inner Image',
			'name' => 'inner_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'project',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_61239b459c7755',
	'title' => 'Post',
	'fields' => array(
		array(
			'key' => 'field_61239b50daeae33',
			'label' => 'Introduction',
			'name' => 'introduction',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae3333',
			'label' => 'Left Column Content',
			'name' => 'left_column_content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae33333',
			'label' => 'Right Column Content',
			'name' => 'right_column_content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daccc',
			'label' => 'Background Image',
			'name' => 'background',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daddd',
			'label' => 'Background Color',
			'name' => 'color',
			'type' => 'color_picker',
			'enable_opacity' => 1,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50dadde',
			'label' => 'Text Color',
			'name' => 'text_color',
			'type' => 'true_false',
			'ui' => 1,
			'ui_on_text' => 'Dark Text',
			'ui_off_text' => 'Light Text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '0',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'service',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sector',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'blog',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_61239b459c77556',
	'title' => 'Content',
	'fields' => array(
		array(
			'key' => 'group_61239b459c775561',
			'label' => 'Contact Section',
			'name' => 'contact_section',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'group_61239b459c775562',
			'label' => 'Contact Logo',
			'name' => 'contact_logo',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b459c775563a',
			'label' => 'Guide Logo',
			'name' => 'guide_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'group_61239b459c775563',
			'label' => 'Guide PDF',
			'name' => 'guide_pdf',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'group_61239b459c775565',
			'label' => 'Team Shortcode',
			'name' => 'team_shortcode',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'group_61239b459c775566',
			'label' => 'Team Shortcode Background Color',
			'name' => 'team_bg_color',
			'type' => 'color_picker',
			'enable_opacity' => 1,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'group_61239b459c775564',
			'label' => 'Insights Shortcode',
			'name' => 'insights_shortcode',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'service',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sector',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_61239b459c8866',
	'title' => 'Blog',
	'fields' => array(
		array(
			'key' => 'field_61239b50daeee',
			'label' => 'Background Color',
			'name' => 'color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'blog',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_61239b459c7755967',
	'title' => 'Post Author/Person',
	'fields' => array(
		array(
			'key' => 'group_61239b459c77559671',
			'label' => 'Post Person',
			'name' => 'post_person',
			'type' => 'post_object',
			'post_type' => ['people'],
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'blog',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));
acf_add_local_field_group(array(
	'key' => 'group_61239b459c7712',
	'title' => 'Slider Color',
	'fields' => array(
		array(
			'key' => 'field_61239b50daeae12',
			'label' => 'Slider Color',
			'name' => 'slider_color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_61239b50daeae13',
			'label' => 'Slider Background Image',
			'name' => 'bg_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'testimonial',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));
endif;
add_action('wp_head', 'start_session_for_fade');
function start_session_for_fade(){
    session_start();
    $_SESSION["fade_cnt"] = 0;
}
