<?php
/*
SCSS Compiler interface 
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

//CHECK URL PARAMETERS AND REACT ACCORDINGLY
add_action("admin_init", function (){
	if (!current_user_can("administrator")) return; //ADMINS ONLY
	
	if (isset($_GET['ps_compile_scss'])) {		config_picostrap_generate_css();		die();	}
	if (isset($_GET['ps_reset_theme'])) {		remove_theme_mods(); 	echo ("Theme Options Reset.<br>");	config_picostrap_generate_css();		die(); }
	if (isset($_GET['ps_show_mods'])){		print_r(get_theme_mods());		wp_die();	}
});

// USE LEAFO's SCSSPHP LIBRARY
use ScssPhp\ScssPhp\Compiler; //https://scssphp.github.io/scssphp/docs/

/////FUNCTION TO GET ACTIVE SCSS CODE FROM FILE ///////
function config_picostrap_get_active_scss_code(){
	
	//INIT WP FILESYSTEM 
	global $wp_filesystem;
	if (empty($wp_filesystem)) {
		require_once (ABSPATH . '/wp-admin/includes/file.php');
		WP_Filesystem();
	}
	
	//READ THE FILE
	// $the_scss_code = $wp_filesystem->get_contents('../wp-content/themes/'.get_stylesheet().'/sass/main.scss');  
	$the_scss_code = $wp_filesystem->get_contents(WP_CONTENT_DIR . '/themes/'.get_stylesheet().'/sass/main.scss');  

	//FOR STYLE PACKAGES
	if(function_exists("picostrap_alter_scss")) $the_scss_code = picostrap_alter_scss ($the_scss_code);	 
	
	return $the_scss_code;
}

 
/////FUNCTION TO RECOMPILE THE CSS ///////
function config_picostrap_generate_css(){
	
	//INITIALIZE COMPILER
	require_once "scssphp/scss.inc.php";
	$scss = new Compiler();
	
	try {
		//SET IMPORT PATH: CURRENTLY ACTIVE THEME's SASS FOLDER
		$scss->setImportPaths(WP_CONTENT_DIR.'/themes/'.get_stylesheet().'/sass/');

		//IF USING A CHILD THEME, add parent theme sass folder: picostrap
		if (is_child_theme()) $scss->addImportPath(WP_CONTENT_DIR.'/themes/'.picostrap_get_active_parent_theme_slug().'/sass/');
		
		//add extra path for style packages
		if(function_exists("picostrap_add_scss_import_path")) $scss->addImportPath(picostrap_add_scss_import_path());
		
		//SET OUTPUT FORMATTING
		$scss->setFormatter('ScssPhp\ScssPhp\Formatter\Crunched');
		
		// ENABLE SOURCE MAP // ADD OPTION
		//$scss->setSourceMap(Compiler::SOURCE_MAP_INLINE);
		
		//SET SCSS VARIABLES
		$scss->setVariables(picostrap_get_active_scss_variables_array());
		
		//NOW COMPILE
		$compiled_css = $scss->compile(config_picostrap_get_active_scss_code());
	
	} catch (Exception $e) {
		//COMPILER ERROR: TYPICALLY INVALID SCSS CODE
		die("<div id='compile-error' style='font-size:20px;background:#212337;color:lime;font-family:courier;border:8px solid red;padding:15px;display:block'><h1>SCSS error</h2>".$e->getMessage()."</div>");
   	}
	
	//CHECK CSS IS REALLY THERE
	if ($compiled_css=="") die("Compiled CSS is empty, aborting.");
	
	//ADD SOME COMMENT
	$compiled_css .= " /* DO NOT ADD YOUR CSS HERE. ADD IT TO SASS/_CUSTOM.SCSS */ ";

	//INIT WP FILESYSTEM 
	global $wp_filesystem;
	if (empty($wp_filesystem)) {
		require_once (ABSPATH . '/wp-admin/includes/file.php');
		WP_Filesystem();
	}

	//SAVE THE FILE
	// $saving_operation = $wp_filesystem->put_contents('../wp-content/themes/'.get_stylesheet() . '/' . picostrap_get_css_optional_subfolder_name() . picostrap_get_complete_css_filename(), $compiled_css, FS_CHMOD_FILE ); // , 0644 ?
	$saving_operation = $wp_filesystem->put_contents(WP_CONTENT_DIR . '/themes/'.get_stylesheet() . '/' . picostrap_get_css_optional_subfolder_name() . picostrap_get_complete_css_filename(), $compiled_css, FS_CHMOD_FILE ); // , 0644 ?
	
	if ($saving_operation) { // IF UPLOAD WAS SUCCESSFUL 

		//SET TIMESTAMP
		set_theme_mod("picostrap_scss_last_filesmod_timestamp",picostrap_get_scss_last_filesmod_timestamp());

		//GIVE POSITIVE FEEDBACK	
		if (isset($_GET['ps_compiler_api'])) {
			echo "New CSS bundle: " . picostrap_get_css_url();
		} else {		
			echo "File was successfully uploaded<br><br>";
			echo "<a href='".picostrap_get_css_url()."' target='new'>View File</a>";
			echo "<br><br><b>Size: </b><br>".round(mb_strlen($compiled_css, '8bit')/1000)." kB - ".round(mb_strlen(gzcompress($compiled_css), '8bit')/1000)." kB gzipped";
		}

	} else {
		//GIVE NEGATIVE FEEDBACK
		if (isset($_GET['ps_compiler_api'])) {
			echo  "<br><br><span id='saving-error'>Error saving CSS file "."</span>";
		} else {
			echo  "<div id='savingfile-error' style='font-size:20px;background:#212337;color:lime;font-family:courier;border:8px solid red;padding:15px;display:block'><h1>Error writing file</h1></div>";
		die();
		}
	}
 
	//PRINT A CLOSE BUTTON
	if (!isset($_GET['ps_compiler_api'])) echo  " <button style='font-size:30px;width:100%' class='cs-close-compiling-window'>OK </button>";
}
