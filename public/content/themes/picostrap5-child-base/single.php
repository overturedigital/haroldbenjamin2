<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();




if ( have_posts() ) : 
    while ( have_posts() ) : the_post();
    
    ?>

    <div class="new-project-header mb-4" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>);">
        <div class="new-project-header-meta">
            <h6 class="new-project-date">Date // <?php the_date(); ?></h6>
            <h6 class="new-project-author">Author // <?php the_author(); ?></h6>

            <h1 class="new-project-title"><?php the_title(); ?></h1>
        </div>
    </div>

    <div class="container main-content">
        <div class="row">
            <div class="col-sm-12">
                <?php the_content(); ?>
            </div>
        </div>
    </div>

<?php
    endwhile;
 else :
     _e( 'Sorry, no posts matched your criteria.', 'picostrap' );
 endif;
 ?>


 

<?php get_footer();
